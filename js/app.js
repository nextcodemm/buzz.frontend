// Default colors
var brandPrimary = '#20a8d8';
var brandSuccess = '#4dbd74';
var brandInfo = '#63c2de';
var brandWarning = '#f8cb00';
var brandDanger = '#f86c6b';

var grayDark = '#2a2c36';
var gray = '#55595c';
var grayLight = '#818a91';
var grayLighter = '#d1d4d7';
var grayLightest = '#f8f9fa';

angular
  .module('app', [

    'ngGuid',
    'ngAudio',
    'ui.router',
    'oc.lazyLoad',
    'ncy-angular-breadcrumb',
    'angular-loading-bar',

    'mm.acl',
    'moment-picker',
    'ngFileUpload',
    'ui.filters',
    'color.picker',
    "ngSanitize",
    'ngToast',
    'rt.select2',
    'ui.pagedown',
    'ngTagsInput'
  ])
  .constant('urls', {
    BASE: 'https://cloud.nextcodesoftware.com',
    // USER_SERVICE_API: 'http://172.30.0.242:81/api/'
    // USER_SERVICE_API: 'http://192.168.2.31:81/api/'
    //USER_SERVICE_API: 'http://localhost:81/api/'
    // USER_SERVICE_API: 'http://192.168.10.2:81/api/'
    //  USER_SERVICE_API: 'http://app.nextcodeserver.com:81/api/'
    USER_SERVICE_API: 'http://52.221.180.34:60009/api/'
    // USER_SERVICE_API: 'http://172.30.0.242:81/api/'
    //  USER_SERVICE_API : 'http://localhost:49479/api/'
    // USER_SERVICE_API: 'http://localhost:81/api/'
    // USER_SERVICE_API: 'http://192.168.0.1:81/api/'
    //USER_SERVICE_API: 'http://172.20.10.5:81/api/'
  })
  .constant('Auth', {
    isLoggedIn: function () {
      var isLogin = localStorage.getItem("currentuser");
      if (isLogin) {
        return true;
      }
      return false;
    },
    getCurrentUser: function () {
      return JSON.parse(localStorage.getItem("currentuser"));
    }
  })
  .config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
    cfpLoadingBarProvider.latencyThreshold = 1;


  }])
  .config(['$compileProvider',
    function ($compileProvider) {
      $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):/);
    }])
  .filter('fromNow', function () {
    return function (input) {
      return moment(input).locale(window.navigator.language).fromNow();
    }
  })
  .run(['$rootScope', '$state', '$stateParams', '$location', '$http', 'AclService', '$transitions', 'ngAudio', 'ngToast',
    function ($rootScope, $state, $stateParams, $location, $http, AclService, $transitions, ngAudio, ngToast) {
      $rootScope.$on('$stateChangeSuccess', function () {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
      });

      //$rootScope.globals = $cookies.getObject('globals') || {};
      $rootScope.globals = {};

      $rootScope.isUseLogo = false;

      $rootScope.isLoggedIn = function () {
        var isLogin = localStorage.getItem("currentuser");
        if (isLogin) {
          return true;
        }
        return false;
      }

      $rootScope.getCurrentUser = function () {
        return JSON.parse(localStorage.getItem("currentuser"));
      }

      $rootScope.logout = function () {
        localStorage.removeItem("currentuser");
        $state.go("appSimple.questionindex");
        
      }

      $rootScope.countercategory = [{ id: 1, name: "Cashier Queue" }, { id: 2, name: "Pharmacy Queue" }];

      $rootScope.$on('$locationChangeStart', function (event, next, current) {
        // redirect to login page if not logged in and trying to access a restricted page
        //var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;

        // var loggedIn = $rootScope.globals.currentUser;  
        // if (!loggedIn) {
        //   $location.path('/login');
        // } else if (loggedIn.healthCareUnit == 'shcunit') {
        //   $location.path('/selecthealthcareunit');
        // }

      });

      $rootScope.success = function (msg) {
        ngToast.success({ timeout: 1000, content: '<span class="toast">' + msg + '</span>' });
      };

      $rootScope.info = function (msg) {
        ngToast.info({ timeout: 1000, content: '<span class="toast">' + msg + '</span>' });
      };

      $rootScope.warning = function (msg) {
        ngToast.warning({ timeout: 1000, content: '<span class="toast">' + msg + '</span>' });
      };

      $rootScope.danger = function (msg) {
        ngToast.danger({ timeout: 1000, content: '<span class="toast">' + msg + '</span>' });
      };

      $rootScope.$state = $state;
      return $rootScope.$stateParams = $stateParams;
    }]);
