angular.module('app')
    .controller('QuestionsShowCtrl', function ($scope, $http, $location, Auth, $stateParams, urls) {
        $scope.user;
        $scope.answer;
        $scope.question;
        if (!Auth.isLoggedIn()) {
            $location.path('/login');
            $location.replace();
            return;
        }
        $scope.user = Auth.getCurrentUser();
        var loadQuestions = function () {
            $http.get(urls.USER_SERVICE_API + '/posts/getpostbypostid/' + $stateParams.id).then(function (res) {
                if (res.data.posts) {
                    $scope.question = res.data.posts;
                    $http.get(urls.USER_SERVICE_API + '/posts/getallanswers/' + $scope.question.postid).then(function (res) {
                        if (res.data.posts) {
                            $scope.question.answers = res.data.posts;
                            $scope.question.answers.forEach(answer => {
                                $http.get(urls.USER_SERVICE_API + '/comments/' + answer.postid).then(function (res) {
                                    if (res.data.comments) {
                                        answer.comments = res.data.comments;
                                    }
                                });

                            });

                        }
                    });

                    $http.get(urls.USER_SERVICE_API + '/comments/' + $scope.question.postid).then(function (res) {
                        if (res.data.comments) {
                            $scope.question.comments = res.data.comments;
                        }
                    });
                }
            });
        };
        loadQuestions();

        $scope.newAnswer = {};
        $scope.submitAnswer = function () {

            var posts = new FormData();
            posts.append('posttypeid', 2);
            posts.append('owneruserid', $scope.user.userid);
            posts.append('ownerdisplayname', $scope.user.name);
            posts.append('body', $scope.newAnswer.body);
            posts.append('title', $scope.newAnswer.title);
            posts.append('parentid', $scope.question.postid)

            $http.post(urls.USER_SERVICE_API + "/posts/save", posts, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(
                function () {
                    loadQuestions();
                    $scope.newAnswer = {};
                });
        };

        $scope.accepted = function(answer)
        {
            $http.post(urls.USER_SERVICE_API + "/posts/rightanswer/" + answer.postid + "/" + answer.parentid + "/" + answer.owneruserid, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(
                function () {
                    loadQuestions();
                    $scope.newAnswer = {};
                });
        }

        $scope.changeVote = function (post, flag) {
            // $scope.vote = vote == flag ? 'None' : flag;
            $scope.vote = flag;
            if ($scope.vote == "up") {

                $http.post(urls.USER_SERVICE_API + "/posts/upvotes/" + post.owneruserid + "/" + post.postid + "/" + $scope.user.userid, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(
                    function () {
                        loadQuestions();
                        $scope.newAnswer = {};
                    });

            }
            else if ($scope.vote == "down") {

                $http.post(urls.USER_SERVICE_API + "/posts/downvotes/" + post.owneruserid + "/" + post.postid + "/" + $scope.user.userid, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(
                    function () {
                        loadQuestions();
                        $scope.newAnswer = {};
                    });

            }

        };

        $scope.deleteQuestion = function () {
            $http.delete(urls.USER_SERVICE_API + '/posts/delete/' + $stateParams.id).then(function () {
                $location.path('/');
            });
        };
        $scope.deleteAnswer = function (answer) {
            $http.delete(urls.USER_SERVICE_API + '/posts/delete/' + answer.postid).then(function () {
                loadQuestions();
            });
        };
        $scope.updateQuestion = function () {

            var posts = new FormData();

            posts.append('postid', $scope.question.postid);
            posts.append('posttypeid', 1);

            posts.append('acceptedanswerid', $scope.question.acceptedanswerid);
            post.append('creationdate', $scope.question.creationdate);
            post.append('score', $scope.question.score);
            post.append('viewcount', $scope.question.viewcount);
            post.append('lasteditoruserid', $scope.question.lasteditoruserid);
            post.append('lasteditordisplayname', $scope.question.lasteditordisplayname);
            post.append('lasteditdate', $scope.question.lasteditdate);
            post.append('lastactivitydate', $scope.question.lastactivitydate);

            post.append('answercount', $scope.question.answercount);
            post.append('commentcount', $scope.question.commentcount);
            post.append('favoritecount', $scope.question.favoritecount);
            post.append('closeddate', $scope.question.closeddate);
            post.append('communityowneddate', $scope.question.communityowneddate);

            posts.append('tags', $scope.question.tags);
            posts.append('owneruserid', $scope.user.userid);
            posts.append('ownerdisplayname', $scope.user.name);
            posts.append('body', $scope.question.body);
            posts.append('title', $scope.question.title);

            $http.post(urls.USER_SERVICE_API + "/posts/save", posts, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(
                function () {
                    loadQuestions();
                });
        };
        $scope.updateAnswer = function (answer) {

            var posts = new FormData();

            posts.append('postid', answer.postid);
            posts.append('posttypeid', 2);
            posts.append('acceptedanswerid', answer.acceptedanswerid);
            post.append('creationdate', answer.creationdate);
            post.append('score', answer.score);
            post.append('viewcount', answer.viewcount);
            post.append('lasteditoruserid', answer.lasteditoruserid);
            post.append('lasteditordisplayname', answer.lasteditordisplayname);
            post.append('lasteditdate', answer.lasteditdate);
            post.append('lastactivitydate', answer.lastactivitydate);
            post.append('tags', answer.tags);
            post.append('answercount', answer.answercount);
            post.append('commentcount', answer.commentcount);
            post.append('favoritecount', answer.favoritecount);
            post.append('closeddate', answer.closeddate);
            post.append('communityowneddate', answer.communityowneddate);

            posts.append('owneruserid', $scope.user.userid);
            posts.append('ownerdisplayname', $scope.user.name);
            posts.append('body', $scope.newAnswer.body);
            posts.append('title', $scope.newAnswer.title);
            posts.append('parentid', $scope.question.postid)

            $http.post(urls.USER_SERVICE_API + "/posts/save", posts, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(
                function () {
                    loadQuestions();
                    $scope.newAnswer = {};
                });

            // $http.put('/api/questions/' + $stateParams.id + '/answers/' + answer._id, answer).success(function () {
            //     loadQuestions();
            // });
        };
        $scope.isOwner = function (obj) {
            return Auth.isLoggedIn() && obj && obj.owneruserid && obj.owneruserid.toUpperCase() === Auth.getCurrentUser().userid.toUpperCase();
        };

        $scope.isOwnerComment = function (obj) {
            return Auth.isLoggedIn() && obj && obj.userid && obj.userid.toUpperCase() === Auth.getCurrentUser().userid.toUpperCase();
        };


        $scope.newComment = {};
        $scope.submitComment = function () {

            var comments = new FormData();
            // comments.append('commentid',)
            comments.append('postid', $stateParams.id)
            // comments.append('score',)
            comments.append('text', $scope.newComment.text)
            // comments.append('creationdate',)
            comments.append('userdisplayname', $scope.user.name)
            comments.append('userid', $scope.user.userid)

            $http.post(urls.USER_SERVICE_API + "/comments/save", comments, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(
                function () {
                    loadQuestions();
                    $scope.newComment = {};
                    $scope.editNewComment = false;
                });
        };
        $scope.submitAnswerComment = function (answer) {
            var comments = new FormData();
            // comments.append('commentid',)
            comments.append('postid', answer.postid)
            // comments.append('score',)
            comments.append('text', answer.newAnswerComment.text)
            // comments.append('creationdate',)
            comments.append('userdisplayname', $scope.user.name)
            comments.append('userid', $scope.user.userid)

            $http.post(urls.USER_SERVICE_API + "/comments/save", comments, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(
                function () {
                    loadQuestions();
                });
        };
        $scope.deleteComment = function (comment) {

            $http.delete(urls.USER_SERVICE_API + 'comments/delete/' + comment.commentid).then(function () {
                loadQuestions();
            });
        };
        $scope.deleteAnswerComment = function (answer, answerComment) {
            $http.delete(urls.USER_SERVICE_API + 'comments/delete/' + answerComment.commentid).then(function () {
                loadQuestions();
            });
        };
        $scope.updateComment = function (comment) {

            var comments = new FormData();
            comments.append('commentid', comment.commentid);
            comments.append('postid', $stateParams.id);
            comments.append('score', comment.score);
            comments.append('text', comment.text);
            comments.append('creationdate', comment.creationdate);
            comments.append('userdisplayname', $scope.user.name);
            comments.append('userid', $scope.user.userid);

            $http.post(urls.USER_SERVICE_API + "/comments/save", comments, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(
                function () {
                    loadQuestions();
                });
        };
        $scope.updateAnswerComment = function (answer, answerComment) {
            var comments = new FormData();
            comments.append('commentid', answerComment.commentid);
            comments.append('postid', $stateParams.id);
            comments.append('score', answerComment.score);
            comments.append('text', answerComment.text);
            comments.append('creationdate', answerComment.creationdate);
            comments.append('userdisplayname', $scope.user.name);
            comments.append('userid', $scope.user.userid);

            $http.post(urls.USER_SERVICE_API + "/comments/save", comments, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(
                function () {
                    loadQuestions();
                });
        };

        $scope.isStar = function (obj) {
            return Auth.isLoggedIn() && obj && (obj.acceptedanswerid != null);
        };
        $scope.star = function (subpath) {
            $http.put('/api/questions/' + $scope.question._id + subpath + '/star').success(function () {
                loadQuestions();
            });
        };
        $scope.unstar = function (subpath) {
            $http.delete('/api/questions/' + $scope.question._id + subpath + '/star').success(function () {
                loadQuestions();
            });
        };
    });