angular.module('app')
    .controller('QuestionsCreateCtrl', function ($scope, $http, $window, $location, Auth, urls, $state) {
        $scope.user;
        $scope.question;
        if (!Auth.isLoggedIn()) {
            $location.path('/login');
            $location.replace();
            return;
        }
        $scope.user = Auth.getCurrentUser();
        console.log("User");
        console.log($scope.user);
        $scope.submit = function () {
            console.log($scope.question);

            $scope.question.tags = "";
            $scope.question.tagList.forEach(tag => {
                $scope.question.tags += tag.text + " ";
            });

            var posts = new FormData();
            posts.append('posttypeid', 1);
            posts.append('tags', $scope.question.tags);
            posts.append('owneruserid', $scope.user.userid);
            posts.append('ownerdisplayname', $scope.user.name);
            posts.append('body', $scope.question.body);
            posts.append('title', $scope.question.title);

            $http.post(urls.USER_SERVICE_API + "/posts/save", posts, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(
                function (response) {
                    $window.alert("save successful")
                    $state.go('appSimple.questionindex');
                }, function (error) {
                    $window.alert("can't save question" + error)
                }
            )

            // $http.post('/api/questions', $scope.question).success(function(){
            //   $location.path('/');
            // });
        };
    });