angular.module('app')
    .controller('QuestionsIndexCtrl', function ($scope, $http, $location, Auth, urls, $stateParams) {
        var keyword = $location.search().keyword;
        // if (keyword) {
        //     query = _.merge(query, { $text: { $search: keyword } });
        // }



        $scope.busy = true;
        $scope.noMoreData = false;

        console.log("param-");
        console.log($stateParams.id);
        if($stateParams.id)
        {
            $http.get( urls.USER_SERVICE_API + '/posts/getallquestionbyuserid/' + $stateParams.id ).then(function (res) {
                if(res)
                {
                    $scope.questions = res.data.posts;
                    console.log($scope.questions);
                }
                $scope.busy = false;
                
            });

        }
        else
        {
            $http.get( urls.USER_SERVICE_API + '/posts/getallposts/1').then(function (res) {
                if(res)
                {
                    $scope.questions = res.data.posts;
                    console.log($scope.questions);
                    // if ($scope.questions.length < 20) {
                    //     $scope.noMoreData = true;
                    // }
                }
                $scope.busy = false;
                
            });
        }

        
        $scope.nextPage = function () {
            // if ($scope.busy) { return; }
            // $scope.busy = true;
            // var lastId = $scope.questions[$scope.questions.length - 1]._id;
            // var pageQuery = _.merge(query, { _id: { $lt: lastId } });
            // $http.get('/api/questions', { params: { query: pageQuery } }).success(function (questions) {
            //     $scope.questions = $scope.questions.concat(questions);
            //     $scope.busy = false;
            //     if (questions.length === 0) {
            //         $scope.noMoreData = true;
            //     }
            // });
        };
        $scope.isStar = function (obj) {
            return Auth.isLoggedIn() && obj && obj.stars && obj.stars.indexOf(Auth.getCurrentUser()._id) !== -1;
        };
    });
