angular
.module('app')
.config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {
  $stateProvider
  .state('app.icons', {
    url: "/icons",
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Icons'
    }
  })

  .state('appSimple.questionindex', {
    url: '/questionindex',
    templateUrl: 'views/components/questionsIndex.html',
    ncyBreadcrumb: {
      label: 'questionsIndex'
    },
    controller: 'QuestionsIndexCtrl',
    controllerAs: 'QuestionsIndexCtrl',
    params: { id: null },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/questionsIndex.controller.js']
        });
      }]
    }
  })

  .state('appSimple.questionsCreate', {
    url: '/questions/create',
    templateUrl: 'views/components/questionsCreate.html',
    ncyBreadcrumb: {
      label: 'questionsCreate'
    },
    controller: 'QuestionsCreateCtrl',
    controllerAs: 'QuestionsCreateCtrl',
    //params: { doctorid: null },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/questionsCreate.controller.js']
        });
      }]
    }
  })

  .state('appSimple.questionsShow', {
    url: '/questions/show/{id}',
    templateUrl: 'views/components/questionsShow.html',
    ncyBreadcrumb: {
      label: 'questionsShow'
    },
    controller: 'QuestionsShowCtrl',
    controllerAs: 'QuestionsShowCtrl',
    params: { id: null },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/questionsShow.controller.js']
        });
      }]
    }
  })

  .state('app.components.doctorentry', {
    url: '/doctorentry',
    templateUrl: 'views/components/doctorentry.html',
    ncyBreadcrumb: {
      label: 'Doctor Setup'
    },
    controller: 'doctorController',
    controllerAs: 'doctorCtrl',
    params: { doctorid: null },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/doctor.js']
        });
      }]
    }
  })

.state('app.components.hospital', {
    url: '/hospital',
    templateUrl: 'views/components/hospital.html',
    ncyBreadcrumb: {
      label: 'Doctor Setup'
    },
    controller: 'healthcareController',
    controllerAs: 'healthcareCtrl',
    params: { doctorid: null },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/healthcareunit.js']
        });
      }]
    }
  })

  .state('app.components.healthcareunitdoctor', {
    url: '/healthcareunitdoctor/{healthcareunitid}',
    templateUrl: 'views/components/healthcareunitdoctor.html',
    ncyBreadcrumb: {
      label: 'Doctor Setup'
    },
    controller: 'healthcController',
    controllerAs: 'hcCtrl',
    params: { healthcareunitid: null },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/healthcareunitdoctor.js']
        });
      }]
    }
  })

  .state('app.components.doctorlist', {
    url: '/doctorlist',
    templateUrl: 'views/components/doctorlist.html',
    ncyBreadcrumb: {
      label: 'Doctor List'
    },
    controller: 'doctorController',
    controllerAs: 'doctorCtrl',
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/doctor.js']
        });
      }]
    }
  })

  .state('app.components.healthcareunit', {
    url: '/healthcareunit',
    templateUrl: 'views/components/healthcareunit.html',
    ncyBreadcrumb: {
      label: 'Health Care'
    },
    controller: 'healthcareController',
    controllerAs: 'healthcareCtrl',
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/healthcareunit.js']
        });
      }]
    }
  })

  .state('app.components.healthcareunitentry', {
    url: '/healthcareunitentry',
    templateUrl: 'views/components/healthcareunitentry.html',
    ncyBreadcrumb: {
      label: 'Health Care'
    },
    controller: 'healthcareController',
    controllerAs: 'healthcareCtrl',
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/healthcareunit.js']
        });
      }]
    }
  })

  .state('app.components.category', {
    url: '/category',
    templateUrl: 'views/components/category.html',
    ncyBreadcrumb: {
      label: 'Category Setup'
    },
    controller: 'categoryController',
    controllerAs: 'categoryCtrl',
    params: { categoryid: null },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/category.js']
        });
      }]
    }
  })
  .state('app.components.categorylist', {
    url: '/categorylist',
    templateUrl: 'views/components/categorylist.html',
    ncyBreadcrumb: {
      label: 'Category List'
    },
    controller: 'categoryListController',
    controllerAs: 'categoryListCtrl',
    params: { templateid: null },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/categorylist.js']
        });
      }]
    }
  })

  .state('app.icons.fontawesome', {
    url: '/font-awesome',
    templateUrl: 'views/icons/font-awesome.html',
    ncyBreadcrumb: {
      label: 'Font Awesome'
    }
  })
  .state('app.icons.simplelineicons', {
    url: '/simple-line-icons',
    templateUrl: 'views/icons/simple-line-icons.html',
    ncyBreadcrumb: {
      label: 'Simple Line Icons'
    }
  })
  .state('app.components', {
    url: "/components",
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Components'
    }
  })
  .state('app.components.buttons', {
    url: '/buttons',
    templateUrl: 'views/components/buttons.html',
    ncyBreadcrumb: {
      label: 'Buttons'
    }
  })
  .state('app.components.social-buttons', {
    url: '/social-buttons',
    templateUrl: 'views/components/social-buttons.html',
    ncyBreadcrumb: {
      label: 'Social Buttons'
    }
  })
  .state('app.components.cards', {
    url: '/cards',
    templateUrl: 'views/components/cards.html',
    ncyBreadcrumb: {
      label: 'Cards'
    }
  })
  .state('app.components.forms', {
    url: '/forms',
    templateUrl: 'views/components/forms.html',
    ncyBreadcrumb: {
      label: 'Forms'
    }
  })
  .state('app.components.switches', {
    url: '/switches',
    templateUrl: 'views/components/switches.html',
    ncyBreadcrumb: {
      label: 'Switches'
    }
  })
  .state('app.components.tables', {
    url: '/tables',
    templateUrl: 'views/components/tables.html',
    ncyBreadcrumb: {
      label: 'Tables'
    }
  })
  .state('app.forms', {
    url: '/forms',
    templateUrl: 'views/forms.html',
    ncyBreadcrumb: {
      label: 'Forms'
    },
    resolve: {
      loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) {
        return $ocLazyLoad.load([
          {
            serie: true,
            files: ['js/libs/moment.min.js']
          },
          {
            serie: true,
            files: ['js/libs/daterangepicker.min.js', 'js/libs/angular-daterangepicker.min.js']
          },
          {
            files: ['js/libs/mask.min.js']
          },
          {
            files: ['js/libs/select.min.js']
          }
        ]);
      }],
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/forms.js']
        });
      }]
    }
  })
  .state('app.widgets', {
    url: '/widgets',
    templateUrl: 'views/widgets.html',
    ncyBreadcrumb: {
      label: 'Widgets'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/widgets.js']
        });
      }]
    }
  })
  .state('app.charts', {
    url: '/charts',
    templateUrl: 'views/charts.html',
    ncyBreadcrumb: {
      label: 'Charts'
    },
    resolve: {
      // Plugins loaded before
      // loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) {
      //     return $ocLazyLoad.load([
      //         {
      //             serial: true,
      //             files: ['js/libs/Chart.min.js', 'js/libs/angular-chart.min.js']
      //         }
      //     ]);
      // }],
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/charts.js']
        });
      }]
    }
  })
}]);
